"""library.py for textparser app

Author: James Hohman
Copyright James Hohman 2015
"""
from collections import Counter
from django.contrib import messages
import HTMLParser
import string
from textparser.models import Word  # @UnresolvedImport

# Global.
# replace punctuation instead of remove
tr = string.maketrans(string.punctuation, ' ' * len(string.punctuation))


def parseText(request, text):
    '''Parse text function. Set session data.'''
    request.session['text'] = text
    jsonDict = {}

    # Unescape any html encoded characters for processing.
    html_parser = HTMLParser.HTMLParser()
    text = html_parser.unescape(text)

    # Convert to ascii for simpler processing.
    text = str(text.strip().lower().encode('ascii', 'ignore'))

    # Replace all punctuation with spaces.
    # Translate library is extremely efficient.
    text = text.translate(tr)
    words = text.split()

    counter = Counter()
    excludedWords = []
    for word in words:
        wordObj = Word(word=word)
        if wordObj.validWord():
            counter[wordObj.word] += 1
        else:
            # If it fails validation, we assume it's an illegal word.
            excludedWords.append(word)
    if excludedWords:
        messages.warning(request, (
            'The following words appear to not be real words; '
            'excluded from analysis: %s'
            ) % ', '.join(excludedWords))

    # Store raw values.
    jsonDict['rtotal'] = len(words)
    jsonDict['runique'] = len(counter)
    jsonDict['rdata'] = dict(counter)

    # Set the session data
    request.session['result'] = jsonDict
    return counter


def removeStopWords(request, counter):
    '''Remove stop words and adjust counts.'''
    stopWords = ['a', 'an', 'and', 'are', 'as', 'at', 'be', 'by', 'for',
                 'from', 'has', 'he', 'in', 'is', 'it', 'its', "it's",
                 'of', 'on', 'that', 'the', 'to', 'was', 'were', 'will',
                 'with']
    # Results are cached in the session, retrieve them for processing.
    result = request.session.get('result')
    # Clone raw values, we will remove stop words and adjust the raw values.
    result['total'] = result['rtotal']
    result['unique'] = result['runique']
    result['data'] = result['rdata']

    # Iterate the list of stopWords
    for w in stopWords:
        if w in counter:
            wfreq = counter.pop(w)
            result['data'].pop(w)
            result['unique'] -= 1
            result['total'] -= wfreq

    # Get metrics and correctly sorted data
    result['metrics'], result['data'] = wordMetrics(request, counter)

    # Get the 10 most common words
    result['top'] = result['data'][0:10]
    request.session['result'] = result
    return True


def wordMetrics(request, counter):
    '''Builds a list of word metrics (json ready) and sorts
    the data correctly. Returns a list of metrics and sorted word list. '''
    metricList = []
    wordList = counter.most_common()
    # Normally, this will try to sort ascending key and value. But we need
    # to sort descending value and ascending key, hence the negation.
    wordList = sorted(wordList, key=lambda k: (-k[1], k[0]))
    count = 0
    for wTuple in wordList:
        count += 1
        word, created = Word.objects.get_or_create(  # @UnusedVariable
            word=wTuple[0]
        )

        wlen = len(word.word)
        metricList.append({
            'key': word.word,
            'value': {
                'rank': count,
                'length': wlen,
                'score': word.value,
                'nscore': round(word.value/float(wlen), 2),
                'freq': wTuple[1],
                }
            })

    return metricList, wordList
