/**
 * d3_bar_chart.js
 * Script to render a d3 bar chart for textual analysis.
 *
 * jQuery
 * d3.js
 * @author James Hohman
 * @requires jQuery 1.11.3 or later
 *
 * Copyright (c) 2015, James Hohman
 *
 */

/* global d3:false, alert:false, results:false, alert:false, d3_bar_chart:false, alert:false */

// Namespace the functions via an object to prevent clashes.
(function (d3_bar_chart, $, undefined) {
  // Initialization Function.
  d3_bar_chart.init = function() {
    d3_bar_chart.params = {};
    d3_bar_chart.active = true; // if chart is currently active.
    // Max number of ordinal values
    d3_bar_chart.params.max_ordinal = 20;
    // Ordinal array
    d3_bar_chart.ordinal = [];

    // Build the ordinal array and find the longest word.
    d3_bar_chart.maxWordLen = 0;
    $.each(results.json, function (i, data) {
      if (i >= d3_bar_chart.params.max_ordinal) {
        return false;
      }
      d3_bar_chart.ordinal.push(data.key);
      d3_bar_chart.maxWordLen = d3_bar_chart.maxWordLen < data.value.length ?
        data.value.length : d3_bar_chart.maxWordLen;
    });
  };

  // Resize function
  // Here, "resize" == just rerender the thing. :-)
  d3_bar_chart.resize = function() {
    // only resize if the chart is active.
    if (d3_bar_chart.active) {
      // Redraw the svg canvas
      results.drawCanvas();
      // Draw chart function
      d3_bar_chart.renderChart();
    }
  };

  // Draw chart function
  d3_bar_chart.renderChart = function() {
    d3_bar_chart.max_chars = 20;
    d3_bar_chart.max_chars = d3_bar_chart.maxWordLen > d3_bar_chart.max_chars ?
      d3_bar_chart.max_chars : d3_bar_chart.maxWordLen;
    // Estimate 6 px per char.
    d3_bar_chart.ppc = 9;
    d3_bar_chart.yLabelWidth = d3_bar_chart.max_chars * d3_bar_chart.ppc;

    // Determined the yLabelWidth, so translate the chart to
    // accomodate y-labels
    results.d3.svgG
      .attr(
        'transform', 'translate(' + d3_bar_chart.yLabelWidth +
        ', ' + (-results.params.margin.v) + ')');

    d3_bar_chart.xLabel = 'Frequency';
    d3_bar_chart.yLabel = "Word";
    d3_bar_chart.barWidthRatio = 0.92;
    d3_bar_chart.color = d3.scale.category20c();
    d3_bar_chart.decimalFormat = d3.format("d");

    // Range/Bound Parameters
    d3_bar_chart.yBound = results.d3.svgheight - 2 * results.params.margin.v;
    d3_bar_chart.xBound = results.d3.svgwidth -
      2 * results.params.margin.h - d3_bar_chart.yLabelWidth;
    d3_bar_chart.yRange = d3.scale.ordinal()
      .rangeRoundBands([0, d3_bar_chart.yBound], 0.2, 0.4);
    d3_bar_chart.xRange = d3.scale.linear()
      .range([0, d3_bar_chart.xBound]);

    d3_bar_chart.xAxis = d3.svg.axis()
      .scale(d3_bar_chart.xRange)
      .orient("bottom")
      .tickFormat(d3_bar_chart.decimalFormat)
      .ticks(5);

    d3_bar_chart.yAxis = d3.svg.axis()
      .scale(d3_bar_chart.yRange)
      .orient("left")
      .tickFormat(function (d) {
        if (d.length > d3_bar_chart.max_chars) {
          return d.slice(0, d3_bar_chart.max_chars - 3) + "...";
        }
        else {
          return d;
        }
      });

    // Grid lines
    d3_bar_chart.make_x_axis = function() {
      return d3.svg.axis()
        .scale(d3_bar_chart.xRange)
        .orient("bottom")
        .ticks(5);
    };

    d3_bar_chart.make_y_axis = function() {
      return d3.svg.axis()
        .scale(d3_bar_chart.yRange)
        .orient("left");
      // This function automatically determines the tick interval
      // we can override with .ticks(N)
    };
    // End grid lines

    // Set the x, y domains.
    d3_bar_chart.yRange.domain(d3_bar_chart.ordinal);
    // results.json is sorted. Just pull the first element
    // to retrieve upper bound.
    d3_bar_chart.xRange.domain([0, results.json[0].value.freq]);

    results.d3.svgG
      .append('g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(0, ' + results.d3.svgheight + ')')
        .call(d3_bar_chart.xAxis)
      .append('text')
        .attr('y', -8)
        .attr('x', d3_bar_chart.xBound)
        .style('text-anchor', 'end')
        .text(d3_bar_chart.xLabel);

    // Grid Lines
    results.d3.svgG
      .append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + results.d3.svgheight + ")")
        .call(d3_bar_chart.make_x_axis()
          .tickSize(-d3_bar_chart.yBound, 0, 0)
          .tickFormat("")
        );

    results.d3.svgG
      .append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0, " +
          (2 * results.params.margin.v) +")")
        .call(d3_bar_chart.make_y_axis()
          .tickSize(-(d3_bar_chart.xBound), 0, 0)
          .tickFormat("")
        );
    // End Grid Lines

    // Render the bars
    // Need to modify this to attach the data to the object instead
    // of using jQuery loops.
    results.d3.barsG = results.d3.svgG
      .append('g')
      .attr('class', 'bars')
       .attr('transform', 'translate(0, ' +
           (2 * results.params.margin.v) +')');

    $.each(results.json, function(i, data) {
      // Only render the top N words.
      if (i >= d3_bar_chart.params.max_ordinal) {
        return false;
      }
      // Append the bars
      results.d3.barsG
        .append("rect")
          .attr("class", "bars")
          .attr("id", "bar_" + i)
          .attr("x", d3_bar_chart.xRange(0))
          .attr("y", d3_bar_chart.yRange(data.key))
          .attr("height", d3_bar_chart.yRange.rangeBand())
          .attr("width", d3_bar_chart.xRange(0))
          .style("fill", d3_bar_chart.color(i))
          .style("opacity", 0.9)
          .style("stroke", d3.rgb(d3_bar_chart.color(i)).darker())
          .attr("title", function() {
            if (data.frequency == 1) {
                return 'The word "' + data.key + '" used ' +
                  data.value.freq + ' time';
            }
            else {
                return 'The word "' + data.key + '" used ' +
                  data.value.freq + ' times';
            }
        });
    });

    $.each(results.json, function(i, data) {
      // Only render the top N words.
      if (i >= d3_bar_chart.params.max_ordinal) {
        return false;
      }
      d3.select("#bar_" + i)
        .transition()
          .duration(1000)
          .ease("cubic-in-out")
          .attr("width", d3_bar_chart.xRange(data.value.freq));
    });

    results.d3.svgG
      .append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(0, " +
          (2 * results.params.margin.v) +")")
        .call(d3_bar_chart.yAxis)
      .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text(d3_bar_chart.yLabel);
  };
}(window.d3_bar_chart = window.d3_bar_chart || {}, jQuery));
