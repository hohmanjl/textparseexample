/**
 * results.js
 * Script to present text parser results.
 *
 * jQuery
 * d3.js
 * @author James Hohman
 * @requires jQuery 1.11.3 or later
 *
 * Copyright (c) 2015, James Hohman
 *
 */

/* global d3:false, results:false, d3_bar_chart:false, d3_fl_chart:false, alert:false */

$(document).ready(function() {
  // Resize with a 250 ms throttle
  $(window).resize($.throttle(250, results.resize));
});

// Namespace the functions via an object to prevent clashes.
(function(results, $, undefined) {
  // Function to load json and parameters. If json loads properly,
  // it sets jsonLoaded to true and initializes controls.
  results.jsonLoad = function(json, params) {
    // Initialize any parameters that may have been sent
    // down from the server.
    results.params = typeof params !== 'undefined' ? params : {};
    // Set chart parameter margins
    results.params.margin = {h: 20, v: 25};
    results.params.AR = 0.5;
    results.params.targetId = 'result-body';
    results.params.target = $('#' + results.params.targetId);
    results.d3 = {};
    results.json = json;
    results.jsonLoaded = true;

    // Handle errors in json loading.
    if (results.json === undefined) {
      results.jsonLoaded = false;
      $('div.masthead.clearfix').after(
        '<div class="messages"><div class="alert alert-danger" role="alert">' +
        'Oops, json did not load correctly!</div></div>'
        );
      return;
    }
    if (results.json.hasOwnProperty("key")) {
      if (results.json.key == "error") {
        results.jsonLoaded = false;
        $('div.masthead.clearfix').after(
          '<div class="messages"><div class="alert alert-danger" ' +
          'role="alert">' + results.json.key + ' ' + results.json.value +
          '</div></div>'
          );
        return;
      }
    }
  };

  // resize function for associated scripts
  results.resize = function() {
      d3_fl_chart.resize();
      d3_bar_chart.resize();
  };

  // Helper functions for rendering d3 charts
  results.getTargetWidth = function() {
    return results.params.target.innerWidth();
  };

  results.getTargetHeight = function() {
    return results.params.target.height();
  };

  // Automatically sets height, based upon AR and minimums
  // Width enforced by parent min-width css.
  results.setCanvasSize = function() {
    var w = results.getTargetWidth();
    // Constrain AR
    var h = parseInt(w * results.params.AR);
    results.d3.svg
      .attr('width', '100%')
      .attr('height', h);

    results.d3.svgwidth = parseInt(results.d3.svg.style('width'));
    results.d3.svgheight = parseInt(results.d3.svg.style('height'));
  };

  // Function to render the svg canvas.
  results.drawCanvas = function() {
    results.params.target.empty();
    results.params.target.show();

    results.d3.svg = d3.select('#' + results.params.targetId)
      .append('svg')
        .attr('class', 'd3-chart')
        .attr('id', 'd3-chart-svg');

    results.setCanvasSize();

    // svgG is the "g" transform element (margins)
    results.d3.svgG = results.d3.svg.append('g')
        .attr(
          'transform', 'translate(' +
          results.params.margin.h + ', ' +
          (- results.params.margin.v) + ')'
          );
  };

  // Tab click functionality.
  results.tabClick = function(selector) {
    if (selector === undefined) {
      throw new Error('Selector required.');
    }
    results.activateTab(selector);
    results.renderPanel(selector);
  };

  // Activate tab functionality.
  results.activateTab = function(selector) {
    if (selector === undefined) {
      throw new Error('Selector required.');
    }
    // Deactivate all tabs
    $(selector).parent().siblings().removeClass('active');
    // Activate current tab
    $(selector).parent().addClass('active');
  };

  // Render panel functionality.
  results.renderPanel = function(selector) {
    switch (selector.id) {
        case 'ctrl-table':
          results.renderResultsTable(selector);
          break;
        case 'ctrl-d3-score':
          results.renderScrabbleChart(selector);
          break;
        case 'ctrl-d3-freq':
          results.renderFreqChart(selector);
          break;
        case 'ctrl-word-table':
          results.renderWordResultsTable(selector);
          break;
        default:
          throw new Error('Unknown control.');
    }
  };

  /* The following functions control the panel content display.
   * Just hide the table when it is not needed to prevent having to
   * store and retrieve the table.
   */
  results.renderResultsTable = function(selector) {
    d3_bar_chart.active = false;
    d3_fl_chart.active = false;
    $('#result-heading').text('Result Table');
    $('#result-body').empty().hide();
    $('#result-word-table').hide();
    $('#result-table').show();
  };

  results.renderWordResultsTable = function(selector) {
    d3_bar_chart.active = false;
    d3_fl_chart.active = false;
    $('#result-heading').text('Word List Table');
    $('#result-body').empty().hide();
    $('#result-table').hide();
    $('#result-word-table').show();
  };

  results.renderScrabbleChart = function(selector) {
    d3_bar_chart.active = false;
    d3_fl_chart.active = true;
    $('#result-heading').html($(selector).html() + ' d3.js Chart');
    $('#result-table').hide();
    $('#result-word-table').hide();
    if (!results.jsonLoaded) {
      $('#result-body').html('<p>Missing results data. Cannot render chart!</p>');
      $('#result-body').show();
      return;
    }
    $('#result-body').html('<p>Chart goes here.</p>');
    results.drawCanvas();
    d3_fl_chart.init();
    d3_fl_chart.renderChart();
    d3_fl_chart.force_chart();
  };

  results.renderFreqChart = function(selector) {
    d3_bar_chart.active = true;
    d3_fl_chart.active = false;
    $('#result-heading').text($(selector).html() + ' d3.js Chart');
    $('#result-table').hide();
    $('#result-word-table').hide();
    if (!results.jsonLoaded) {
      $('#result-body').html('<p>Missing results data. Cannot render chart!</p>');
      $('#result-body').show();
      return;
    }
    $('#result-body').html('<p>Chart goes here.</p>');
    results.drawCanvas();
    d3_bar_chart.init();
    d3_bar_chart.renderChart();
  };
}(window.results = window.results || {}, jQuery));
