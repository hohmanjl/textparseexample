/**
 * d3_fl_chart.js
 * Script to render a d3 bar chart for textual analysis.
 *
 * jQuery
 * d3.js
 * @author James Hohman
 * @requires jQuery 1.11.3 or later
 *
 * Copyright (c) 2015, James Hohman
 *
 */

/* global d3:false, results:false, d3_fl_chart:false, alert:false */

// Namespace the functions via an object to prevent clashes.
(function (d3_fl_chart, $, undefined) {
  // Initialization Function.
  d3_fl_chart.init = function() {
    d3_fl_chart.params = {}; // parameters
    d3_fl_chart.d3 = {}; // d3 objects
    d3_fl_chart.active = true; // if chart is currently active.
    /* Special sauce function to build the dataset without looking at
     * kurtosis. Provides a decent cross-section of the data, given
     * typical forms of the dataset.
     * Iterate the entire results.json, but push elements into
     * the d3_fl_chart.json at an increasing interator step above
     * the given threshold.
     */
    var iterThres = 15;
    var iter = 0;
    d3_fl_chart.json = [];
    $.each(results.json, function(i, obj) {
      if (i%iterThres === 0) {
          iter += 1;
          if (iter == 2) {
            // If we increase the iterator >= 2 (skipping values) let
            // the user know that not all data was rendered on this chart.
            $('#d3-chart-svg').before(
              '<p class="warning-note"><span class="glyphicon glyphicon-alert warning-icon"></span> <strong>Note:</strong> For performance reasons we only ' +
              'rendered a subset of the data.</p>'
            );
          }
      }
      if (i%iter === 0) {
          d3_fl_chart.json.push(obj);
      }
    });
    // We define abscissa and ordinate as variables so that we can
    // regenerate charts using different metrics. Not implemented yet.
    d3_fl_chart.params.abscissa = d3_fl_chart.params.abscissa !== undefined ?
      d3_fl_chart.params.abscissa : "freq";
    d3_fl_chart.params.ordinate = d3_fl_chart.params.ordinate !== undefined ?
      d3_fl_chart.params.ordinate : "length";
    // Build the chart foci from the dataset.
    d3_fl_chart.build_foci();
    // Reset the nodes.
    d3_fl_chart.nodes = [];
    // Toggle element to fixed size for a non-overlap version.
    // Not implemented yet.
    d3_fl_chart.overLap = {r: 5};
    // Set the default charge of the nodes.
    d3_fl_chart.defCharge = -10;
  };

  // Resize function
  // Here, "resize" == just rerender the thing. :-)
  d3_fl_chart.resize = function() {
    // only resize if the chart is active.
    if (d3_fl_chart.active) {
      // Redraw the svg canvas
      results.drawCanvas();
      // Render the chart
      d3_fl_chart.renderChart();
      d3_fl_chart.force_chart();
    }
  };

  // Function to set the longest and shortest word.
  d3_fl_chart.setMinMaxWord = function(words) {
    // Init the shortest and longest word
    d3_fl_chart.shortest = {
      'key': words[0].key, 'value': words[0].value.length
      };
    d3_fl_chart.longest = {
      'key': words[0].key, 'value': words[0].value.length
      };
    // set the shortest/longest word
    $.each(words, function(i, w) {
      if (w.value.length < d3_fl_chart.shortest.value) {
        d3_fl_chart.shortest = {'key': w.key, 'value': w.value.length};
        // After initialization a word cannot be both shortest and longest.
        // continue early loop
        return true;
      }
      if (w.value.length > d3_fl_chart.longest.value) {
        d3_fl_chart.longest = {'key': w.key, 'value': w.value.length};
        // After initialization a word cannot be both shortest and longest.
        // continue early loop
        return true;
      }
    });
  };

  // Draw chart function
  /* Right now it displays only one metric. However, it is built
   * such that by changing the d3_fl_chart.params.abscissa or .ordinate
   * we can render the chart using various metrics in the future.
   *
   * Furthermore, much of this code is straight cut-n-paste from
   * d3_bar_chart.js. We should extract the common bits and parameterize
   * the other bits to make a "base chart" rendering function common
   * to both charts. Again, future work.
   */
  d3_fl_chart.renderChart = function() {
    /* there is no yLabel for this chart, but maybe that could change
     * so we'll leave this code for now.
     * However, by default we display word length as the y-label. I do not
     * know of any 3 figure words in English, so let's set wordlen to 2.
     * maxWordLen appears to operate on n + 1. So set it to 3. Investigate
     * and fix later.
     */
    d3_fl_chart.maxWordLen = 3;
    d3_fl_chart.max_chars = 20;
    d3_fl_chart.max_chars = d3_fl_chart.maxWordLen > d3_fl_chart.max_chars ?
      d3_fl_chart.max_chars : d3_fl_chart.maxWordLen;
    // Estimate 6 px per char.
    d3_fl_chart.ppc = 9;
    d3_fl_chart.yLabelWidth = d3_fl_chart.max_chars * d3_fl_chart.ppc;

    // Determined the yLabelWidth, so translate the chart to
    // accomodate y-labels
    results.d3.svgG
      .attr(
        'transform', 'translate(' + d3_fl_chart.yLabelWidth +
        ', ' + (-results.params.margin.v) + ')');

    d3_fl_chart.xLabel = 'Word Frequency';
    d3_fl_chart.yLabel = "Word Length";
    d3_fl_chart.color = d3.scale.category20c();
    d3_fl_chart.decimalFormat = d3.format("d");

    // Range/Bound Parameters
    d3_fl_chart.yBound = results.d3.svgheight - 2 * results.params.margin.v;
    d3_fl_chart.xBound = results.d3.svgwidth -
      2 * results.params.margin.h - d3_fl_chart.yLabelWidth;
    d3_fl_chart.yRange = d3.scale.linear().range([d3_fl_chart.yBound, 0]);
    d3_fl_chart.xRange = d3.scale.linear().range([0, d3_fl_chart.xBound]);

    d3_fl_chart.xAxis = d3.svg.axis()
      .scale(d3_fl_chart.xRange)
      .orient("bottom")
      .tickFormat(d3_fl_chart.decimalFormat)
      .ticks(5);

    d3_fl_chart.yAxis = d3.svg.axis()
      .scale(d3_fl_chart.yRange)
      .orient("left")
      .tickFormat(d3_fl_chart.decimalFormat)
      .ticks(5);

    // Grid lines
    d3_fl_chart.make_x_axis = function() {
      return d3.svg.axis()
        .scale(d3_fl_chart.xRange)
        .orient("bottom")
        .ticks(5);
    };

    d3_fl_chart.make_y_axis = function() {
      return d3.svg.axis()
        .scale(d3_fl_chart.yRange)
        .orient("left")
        .ticks(5);
      // This function automatically determines the tick interval
      // we can override with .ticks(N)
    };
    // End grid lines

    // Set the x, y domains.
    // First find the longest word.
    d3_fl_chart.setMinMaxWord(d3_fl_chart.json);

    // d3_fl_chart.json is sorted. Just pull the first element
    // to retrieve bounds.
    d3_fl_chart.yRange.domain(
      [0.5, d3_fl_chart.longest.value + 0.5]);
    d3_fl_chart.xRange.domain(
      [0.5, d3_fl_chart.json[0].value[d3_fl_chart.params.abscissa] + 0.5]);

    results.d3.svgG
      .append('g')
        .attr('class', 'x axis')
        .attr('transform', 'translate(0, ' + results.d3.svgheight + ')')
        .call(d3_fl_chart.xAxis)
      .append('text')
        .attr('y', -8)
        .attr('x', d3_fl_chart.xBound)
        .style('text-anchor', 'end')
        .text(d3_fl_chart.xLabel);

    // Grid Lines
    results.d3.svgG
      .append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + results.d3.svgheight + ")")
        .call(d3_fl_chart.make_x_axis()
          .tickSize(-d3_fl_chart.yBound, 0, 0)
          .tickFormat("")
        );

    results.d3.svgG
      .append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0, " +
          (2 * results.params.margin.v) +")")
        .call(d3_fl_chart.make_y_axis()
          .tickSize(-(d3_fl_chart.xBound), 0, 0)
          .tickFormat("")
        );
    // End Grid Lines

    // y-axis and labels
    results.d3.svgG
      .append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(0, " +
          (2 * results.params.margin.v) +")")
        .call(d3_fl_chart.yAxis)
      .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text(d3_fl_chart.yLabel);
  };

  // Build foci, the focal point that nodes cluster around.
  d3_fl_chart.build_foci = function() {
    // init the foci array
    d3_fl_chart.foci = [];
    // build the foci
    $.each(d3_fl_chart.json, function (i, obj) {
      // Iterate each data element in the payload
      var insert = true;
      $.each(d3_fl_chart.foci, function (j, f) {
        // If the foci of the element has already been added to
        // the foci array, skip it and flag we are skipping.
        if (
          (f.x == obj.value[d3_fl_chart.params.abscissa]) &&
          (f.y == obj.value[d3_fl_chart.params.ordinate])) {
            insert = false;
            return false;
        }
      });
      // If we have insert flag, then append the data element from
      // the payload into the array
      if (insert) {
        d3_fl_chart.foci.push({
          x: obj.value[d3_fl_chart.params.abscissa],
          y: obj.value[d3_fl_chart.params.ordinate]
        });
      }
    });
  };

  // Attraction Animation Function
  d3_fl_chart.tick = function(e) {
    // Scale the attraction "force" based upon svg size
    var k = Math.sqrt(800 / d3_fl_chart.xBound) * e.alpha;

    // Push nodes toward their designated focus.
    d3_fl_chart.nodes.forEach(function (o, i) {
        o.y += (d3_fl_chart.foci_new[o.id].y - o.y) * k;
        o.x += (d3_fl_chart.foci_new[o.id].x - o.x) * k;
    });

    // for each node element apply an incremental movement.
    d3_fl_chart.node
        .attr("cx", function (d) { return d.x; })
        .attr("cy", function (d) { return d.y; });
  };

  /* Renders the actual force chart bits on top of the
   * d3_fl_chart.renderChart base.
   * Force Layout with Foci
   * http://bl.ocks.org/mbostock/1021953
   * https://github.com/mbostock/d3/wiki/Force-Layout
   */
  d3_fl_chart.force_chart = function() {
    if (d3_fl_chart.json === undefined) {
        // If d3_fl_chart.json has not been
        // populated abort the function.
        return false;
    }

    // This is used to determine tooltip label position in tick function.
    d3_fl_chart.rBound = 0.75 * d3_fl_chart.xBound;

    // deepcopy the array
    // This will translate values to svg coordinates
    d3_fl_chart.foci_new = $.extend([], d3_fl_chart.foci);
    // update foci_new to be in svg range
    $.each(d3_fl_chart.foci, function (i, obj) {
        d3_fl_chart.foci_new[i] = {
          x: d3_fl_chart.xRange(obj.x),
          y: d3_fl_chart.yRange(obj.y)
        };
    });

    var force = d3.layout.force()
        .nodes(d3_fl_chart.nodes)
        .links([])
        .gravity(0)
        .friction(0.75)
        .charge(d3_fl_chart.defCharge)
        //.chargeDistance(d3_fl_chart.xBound * 0.25)
        .chargeDistance(20)
        .size([d3_fl_chart.xBound, d3_fl_chart.yBound])
        .on("tick", d3_fl_chart.tick);

    // What does this if do? <= 0?
    if (d3_fl_chart.nodes.length <= 0) {
        // Add the node objects to d3_fl_chart.nodes where
        // the index is the foci
        // seems to repeat d3_fl_chart.build_foci
        $.each(d3_fl_chart.json, function (i, obj) {
            var index = 0;
            $.each(d3_fl_chart.foci, function (j, f) {
                if (
                  (f.x == obj.value[d3_fl_chart.params.abscissa]) &&
                  (f.y == obj.value[d3_fl_chart.params.ordinate])) {
                    index = j;
                    return false;
                }
            });

            d3_fl_chart.nodes.push({
                id: index, ord: i,
                // Make this dynamically add properties and values.
                // All the below stuff ----------------
                freq: obj.value.freq,
                score: obj.value.score,
                length: obj.value.length,
                nscore: obj.value.nscore,
                rank: obj.value.rank,
                name: obj.key,
                // ------------------------------------
            });
        });
    }

    force.start();

    // Render the nodes
    // Here we used the native d3 feature to attach data directly
    // to the object. This simplifies the manipulation of DOM elements
    // by iterating the embedded dataset.
    // Append the node g container
    d3_fl_chart.node = results.d3.svgG
      .append('g')
        .attr('class', 'nodes')
        .attr('transform', 'translate(0, ' +
          (2 * results.params.margin.v) +')')
        .selectAll("circle").data(
          // Filter the zero valued words.
          d3_fl_chart.nodes.filter(function(d) { return d.score > 0; })
        );

    // append the nodes to the g parent container
    d3_fl_chart.node
      .enter()
      .append("circle")
        .attr("class", "node")
        .attr("cx", function (d) { return d.x; })
        .attr("cy", function (d) { return d.y; })
        .attr("id", function (d) { return "node_" + d.ord; })
        .attr("r", function (d) {
          // Although the function is unbounded I think it gives a better
          // visual representation of scale.
          return 2 * Math.sqrt(d.score); // Unbounded monotonic fx
          // return Math.sqrt((100 * d.score)/(d.score + 19)); // Asymptotic fx
        })
        .style("fill", function (d) { return d3_fl_chart.color(d.ord); })
        .style("stroke", function (d) {
          return d3.rgb(d3_fl_chart.color(d.ord)).darker(1);
        })
        .style("opacity", "0.8")
        .call(force.drag);

    // append a text element. This will be our "tooltip"
    d3_fl_chart.d3.tooltipG = results.d3.svgG
      .append('g')
        .attr('class', 'tooltip');  // Bootstrap sets this class opacity to 0!
    // Move the tooltip off the svg area such that the now transparent
    // text label does not interfere with other nodes.
    d3_fl_chart.d3.tooltipG
      .attr('transform', 'translate(' + (d3_fl_chart.xBound + 100) + ', ' +
        (d3_fl_chart.yBound + 100) + ')');
    d3_fl_chart.d3.tooltip = d3_fl_chart.d3.tooltipG
      .append('text')
        .attr('transform', 'translate(' + 0 + ', ' + 2 * results.params.margin.v + ')')
        .attr('x', 25)
        .attr('y', 25)
        .text('I should be hidden.');

    // Add mouse interactivity.
    d3.selectAll("circle.node")
      .on('mouseover', function(d1, i) {
        // We simply set the tooltip node to "follow" here.
        var offset = 0;
        // set label
        d3_fl_chart.d3.tooltip.text('"' + d1.name + '" frequency: ' +
          d1.freq + ' length: ' + d1.length + ' Scrabble Score: ' + d1.score);

        // if it's past the rBound, shift it to the left side of element.
        if (d1.x > d3_fl_chart.rBound) {
            offset = -0.36 * d3_fl_chart.xBound;
        }
        d3_fl_chart.d3.tooltipG
          .attr('transform', 'translate(' + (d1.x + offset) +
            ', ' + d1.y + ')');

        // nice fade in.
        d3_fl_chart.d3.tooltipG.transition()
                  .duration(1000)
                  .style("opacity", 1);

        // Change node charges on hover
        force.charge(function (d2, i) {
            // d1 is the element under the cursor. for each node (d2)
            // if d2 is the element under the cursor set the charge to be
            // very repulsive. All other elements retain default.
            if (d2.ord == d1.ord) {
                return -d3_fl_chart.xBound;
            } else {
                return d3_fl_chart.defCharge;
            }
        });
        force.start();
      })
      .on('mouseout', function(d) {
        // Hide the tooltip
        d3_fl_chart.d3.tooltipG
          .style("opacity", 0);
        // Move the tooltip off the svg area such that the now transparent
        // text label does not interfere with other nodes.
        d3_fl_chart.d3.tooltipG
          .attr('transform', 'translate(' + (d3_fl_chart.xBound + 100) + ', ' +
            (d3_fl_chart.yBound + 100) + ')');
        force.charge(function() {
          // default charge
          return d3_fl_chart.defCharge;
        });
        force.start();
      });
  };
}(window.d3_fl_chart = window.d3_fl_chart || {}, jQuery));
