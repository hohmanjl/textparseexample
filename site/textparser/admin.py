"""admin.py for textparser app

Author: James Hohman
Copyright James Hohman 2015
"""
from django.contrib import admin
from textparser.models import Letter, Word  # @UnresolvedImport


############################################
# Admin
############################################
class LetterAdmin(admin.ModelAdmin):
    list_display = ('letter', 'value')
    list_filter = ('value',)

    ordering = ('letter',)


class WordAdmin(admin.ModelAdmin):
    list_display = ('word', 'value')
    ordering = ('word',)
    search_fields = ('word',)


############################################
# Register
############################################
admin.site.register(Letter, LetterAdmin)
admin.site.register(Word, WordAdmin)
