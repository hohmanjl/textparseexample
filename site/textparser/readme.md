# textParseExample
## A Python/Django/d3.js Example Web Site
## June 2015
## Copyright (C) James Hohman, 2015
----------------------------------------------

## textparser application

## Description
----------------------------------------------
The textparser application parses input text and returns json response for 
d3.js visualizations.
