"""forms.py for textparser app.

Author: James Hohman
Copyright James Hohman 2015
"""

from django import forms


class TextForm(forms.Form):
    text = forms.CharField(
        label='Input Text', max_length=10000, widget=forms.Textarea(
            attrs={'placeholder': 'Enter text here.',
                   'class': 'form-control required', 'rows': '5'}
            ))
