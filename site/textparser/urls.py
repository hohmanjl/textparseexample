"""urls.py for textparser app

Author: James Hohman
Copyright James Hohman 2015
"""

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'textparser.views',
    url(r'^$', 'index', name='index'),
    url(r'^results/$', 'results', name='results'),
    url(r'^flush/$', 'flush', name='flush'),
    )
