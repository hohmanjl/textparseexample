"""textparser.views test library.

Author: James Hohman
Copyright James Hohman 2015
"""
from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
from django.conf import settings


class IndexTest(TestCase):
    '''Tests index related stuff.'''
    def setUp(self):
        self.client = Client()
        super(IndexTest, self).setUp()

    def test_index_view(self):
        "Tests the index view."
        # Create an instance of a GET request.
        response = self.client.get(reverse('textparser:index'))

        templates = [
            u'common/site_base.html', u'textparser/index.html',
            u'common/messages.html',
            ]
        for template in templates:
            self.assertTemplateUsed(response, template)
        self.assertEqual(response.status_code, 200)

    def test_index_view_context_empty_session(self):
        "Tests the index view's context for a fresh session."
        response = self.client.get(reverse('textparser:index'))
        self.assertContains(
            response,
            '<title>%s (%s)' % (settings.SITE_NAME, settings.RELEASE_VER)
            )
        content = response.content
        self.assertInHTML('<meta name="author" content="J. Hohman.">', content)
        self.assertInHTML('<li><a href="/">Home</a></li>', content)
        self.assertNotContains(
            response,
            '''<li class="active"><a href="{% url 'textparser:results' %}">
            Results</a>''')
        self.assertInHTML(
            '<h3 class="masthead-brand">%s</h3>' % settings.SITE_NAME,
            content
            )

        # Now verify context
        self.assertEqual(response.context['form'].as_p(), (
            u'<p><label for="id_text">Input Text:</label> <textarea '
            u'class="form-control required" cols="40" id="id_text" '
            u'maxlength="10000" name="text" placeholder="Enter text '
            u'here." rows="5">\r\n</textarea></p>'
            ))
        self.assertEqual(response.context['hide_result_link'], True)
