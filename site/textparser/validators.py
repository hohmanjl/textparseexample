"""validators.py for textparser app

Author: James Hohman
Copyright James Hohman 2015
"""
from django.core.exceptions import ValidationError


def IsAlphaValidator(value):
    '''Validates the string is only alpha characters.'''
    if not value.isalpha():
        raise ValidationError('Html tags are not allowed.')
