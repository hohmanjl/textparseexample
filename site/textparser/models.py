"""models.py for textparser app

Author: James Hohman
Copyright James Hohman 2015
"""
from django.db import models
from django.core.exceptions import ValidationError
from textparser.validators import IsAlphaValidator  # @UnresolvedImport


class WordManager(models.Manager):
    def get_or_create(self, *args, **kwargs):
        '''Custom get or create. On validation error returns the object
        but did not create it in db, with value of 0.'''
        created = False
        try:
            obj = Word.objects.get(**kwargs)
        except Word.DoesNotExist:
            obj = Word(**kwargs)
            try:
                obj.full_clean()
            except ValidationError:
                return obj, created
            obj.save()
            created = True

        return obj, created


class Letter(models.Model):
    letter = models.CharField(max_length=1, unique=True)
    value = models.IntegerField()

    def __unicode__(self):
        return self.letter


class Word(models.Model):
    '''The Word model. The longest English word is something like 28-45
    characters long depending how you qualify. Regardless, if it's longer than
    max_length we'll consider the value to be 0 and we won't store it.
    http://en.wikipedia.org/wiki/Longest_word_in_English
    '''
    word = models.CharField(
        max_length=64, unique=True, validators=[IsAlphaValidator])
    value = models.IntegerField(default=0, editable=False)

    objects = WordManager()

    def calcWordValue(self):
        '''Calculates the word value by querying each letter from the db.
        Yes, Letter.objects.all() should be cached somehow for
        performance reasons.'''
        letterQuery = Letter.objects.all()
        value = 0
        for l in self.word:
            try:
                value += letterQuery.get(letter=l).value
            except Letter.DoesNotExist:
                continue

        return value

    def validWord(self):
        '''Based on word length and numeric characters,
        is the word valid or not?'''
        if len(self.word) > 64:
            return False
        elif not self.word.isalpha():
            return False
        else:
            return True

    def save(self, *args, **kwargs):
        '''Calculate the value and save the word.'''
        self.word = self.word.lower()
        self.value = self.calcWordValue()
        super(Word, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.word
