"""views.py for textparser app

Author: James Hohman
Copyright James Hohman 2015
"""
from django.core.exceptions import PermissionDenied
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from textparser.forms import TextForm  # @UnresolvedImport
import json
from textparser.library import (
    parseText, removeStopWords  # @UnresolvedImport
    )


@csrf_protect
def index(request):
    '''Textparser index view. Presents text form on GET. Processes
    text form on POST.'''
    resp_page = 'textparser/index.html'
    rc = {}  # response context.

    if request.method == 'GET':
        results = request.session.get('result')
        if results is None:
            rc['hide_result_link'] = True
            textForm = TextForm()
        else:
            textForm = TextForm(initial={'text': request.session.get('text')})
    elif request.method == 'POST':
        textForm = TextForm(request.POST)
        counter = parseText(request, textForm.data.get('text'))
        removeStopWords(request, counter)
        return HttpResponseRedirect(reverse('textparser:results'))
    else:
        raise PermissionDenied

    rc['form'] = textForm

    return render(request, resp_page, rc)


def results(request):
    '''Textparser results view.'''
    resp_page = 'textparser/result.html'
    rc = {}  # response context.
    result = request.session.get('result')

    # We can clean up these dicts as necessary.
    # Here for the prototype.
    rc['jsonMetrics'] = json.dumps(result['metrics'])
    rc['jsonFreq'] = result['data']
    rc['jsonTop'] = result['top']
    rc['jsonTot'] = result['total']
    rc['jsonUniq'] = result['unique']
    rc['jsonRFreq'] = result['rdata']
    rc['jsonRTot'] = result['rtotal']
    rc['jsonRUniq'] = result['runique']

    return render(request, resp_page, rc)


@csrf_protect
def flush(request):
    '''Flushes the current session.'''
    if request.method != 'POST':
        raise PermissionDenied

    try:
        request.session.flush()
        messages.success(request,
                         "Success! Your session data has been flushed."
                         )
    except:
        messages.success(request,
                         "An error occurred while flushing session data."
                         )

    return HttpResponseRedirect(reverse('textparser:index'))
