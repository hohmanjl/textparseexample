"""library.py for core app

Author: James Hohman
Copyright James Hohman 2015
"""
from random import choice
import os
# import ipdb


def getRandBackground():
    '''Makes random background choice.'''
    return choice(background_list)


def buildBackgroundList():
    '''Build the list of backgrounds for choice.'''
    bList = []
    dirList = os.listdir('core/static/common/img')
    for d in dirList:
        if d.startswith('background_'):
            bList.append(d)
    # ipdb.set_trace()
    return bList

# build the global background list. Should only execute once on startup.
background_list = buildBackgroundList()
