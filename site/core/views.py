"""views.py for core app

Author: James Hohman
Copyright James Hohman 2015
"""
from django.shortcuts import render


def index(request):
    resp_page = 'core/index.html'
    rc = {}

    results = request.session.get('result')
    if results is None:
        rc['hide_result_link'] = True

    return render(request, resp_page, rc)
