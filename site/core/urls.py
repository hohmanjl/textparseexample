"""urls.py for core app

Author: James Hohman
Copyright James Hohman 2015
"""

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'core.views',
    url(r'^$', 'index', name='index'),
    )
