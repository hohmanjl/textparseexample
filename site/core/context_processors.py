"""context_processors.py for core app

Author: James Hohman
Copyright James Hohman 2015
"""
from django.conf import settings
from core.library import getRandBackground  # @UnresolvedImport


# Build site parameters dictionary
siteParams = {'company': settings.COMPANY,
              'current_year': settings.CURRENT_YEAR,
              'title': settings.SITE_NAME,
              'release': settings.RELEASE_VER,
              'tagline': settings.TAG_LINE,
              'cdn': settings.CDN}


def sitePreferences(request):
    """Context processor that returns site globals to be available to
    every request."""
    siteParams['background'] = getRandBackground()
    return {'siteParams': siteParams}
