# textParseExample
## A Python/Django/d3.js Example Web Site
## June 2015
## Copyright (C) James Hohman, 2015
----------------------------------------------

## core application

## Description
----------------------------------------------
Core application stuff common to the site like templates, context processors, 
middleware, etc... Also, application functionality that doesn't fall under 
specific apps can go here.
