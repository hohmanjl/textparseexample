/*  cover.js Javascript/jQuery
 *  James Hohman
 *  Copyright James Hohman, 2015
 *
 *  Functions that dynamically set cover padding when
 *  window is resized. Uses throttle to reduce
 *  unnecessary browser workload.
 */

// globals
// throttle delay
var throttleMS = 250;
var oldMS = new Date().getTime();
// get window height
var winHeight = $(window).height();
var minPaddingHeight = 60;


// Function that sets appropriate padding height for cover divs.
var setPadHeight = function(height) {
  // sets padding height on covers as a function of height.
  var padding = height * 0.25;
  // enforce minimum height.
  padding = padding > minPaddingHeight ? padding : minPaddingHeight;

  // set padding height.
  // The first element is controlled via bootstrap @media
  // The rest we set normally.
  $('.inner.cover:not(:first)').css('padding-top', padding + 'px');
  $('.inner.cover:not(:first)').css('padding-bottom', padding + 'px');

  // Now lets set the first cover position, properly.
  var fcPad = $('.first-cover').css('padding-top');
  var fcHeight = $('.first-cover').height();
  var mastHeight = $('div.masthead').height();
  var mar = (height - mastHeight - fcHeight) * 0.5;

  // Ensure padding is greater than zero.
  mar = mar > 0 ? mar : 0;

  // fcPad is 0px when @media triggered below certain size
  // otherwise, padding is set by css > 0px.
  if (fcPad == "0px") {
    if (mar > 0) {
      $('.first-cover').css('margin-top', mar + 'px');
      $('.first-cover').css('margin-bottom', mar + 'px');
    }
  } else {
    // Adjust per floating footer
    var marAdj = $('div.mastfoot').height()/2;
    // Ensure padding is greater than zero.
    marAdj = marAdj > 0 ? marAdj : 0;
    // Ensure the top padding is never negative.
    var marTop = mar - marAdj;
    marTop = marTop > 0 ? marTop : 0;
    $('.first-cover').css('margin-top', marTop + 'px');
    $('.first-cover').css('margin-bottom', mar + marAdj + 'px');
  }
};

// Function to automatically dismiss system messages after delay
var closeMessages = function() {
  window.setTimeout(function() {
    $("div.alert").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
    });
  }, 5000);
};

// on resize update padding heights.
$( window ).resize(function() {
  // throttle execution
  var newMS = new Date().getTime();
  if (newMS - oldMS > throttleMS) {
    // On window resize check if height changed.
    // Although height might not change, changing
    // width will change element sizes due to responsive
    // design. Thus, we check the padding heights.
    winHeight = $(window).height();
    setPadHeight(winHeight);
  }
});

$( document ).ready(function() {
  // Initialize padding heights on covers.
  setPadHeight(winHeight);
  closeMessages();
});
