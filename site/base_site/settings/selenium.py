'''site/base_site/settings/selenium.py.

Author: James Hohman
Orig. Date: 2015-06-08
Copyright James Hohman 2015

Inherits from development settings and configures site for
selenium testing environment.
'''
# Import all base settings
from base_site.settings.development import *

'''Override base settings with environment specific settings.'''
WSGI_APPLICATION = 'base_site.wsgi_selenium.application'

# Nose testing stuff
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
    '--nocapture',  # print stdout to console, i.e., print statements
    '-A selenium_tests',  # only selenium tests
    '--ipdb',  # drop to ipdb on failure
    # '--with-coverage',
    # '--cover-html-dir=' + os.path.join(BASE_DIR, 'test_results', 'cover'),
    # '--cover-package=base_site,core,textparser,'
    # '--cover-inclusive',
    # '--cover-html',
]

# Since we are in test, display some messages.
print '*' * 22 + ' THIS IS TEST SYSTEM SETTINGS ' + '*' * 21
print '*' * 17 + ' YOU HAVE STARTED THE TEST ENVIRONMENT ' + '*' * 17
