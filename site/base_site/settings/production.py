'''site/base_site/settings/production.py.

Author: James Hohman
Orig. Date: 2015-06-10
Copyright James Hohman 2015

Inherits from base settings and configures site for PRODUCTION environment.
'''
import os
# Import all base settings
from base_site.settings.base import *

'''Override base settings with environment specific settings.'''
WSGI_APPLICATION = 'base_site.wsgi.application'

'''Read sensitive configuration details from configuration file.'''
execfile(os.path.expanduser('../config/.django_textParseExample_settings'))

TEMPLATES[0]['OPTIONS']['context_processors'] += [  # @UndefinedVariable
    'django.core.context_processors.static',
]

# Site Roots
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')  # @UndefinedVariable
STATIC_ROOT = os.path.join(BASE_DIR, 'static')  # @UndefinedVariable

# Auth Stuff
SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False

ALLOWED_HOSTS = ['localhost']

# Display debug message if debug is on.
if DEBUG:  # @UndefinedVariable
    print '*' * 30 + ' DEBUG IS ON ' + '*' * 30
