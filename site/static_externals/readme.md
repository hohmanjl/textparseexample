# textParseExample
## A Python/Django/d3.js Example Web Site
## June 2015
## Copyright (C) James Hohman, 2015
----------------------------------------------

## static_externals

The "static_externals" hold static content that is normally served via a CDN 
server. Sometimes, we need to serve content from our web server when, for 
example, we don't have network connectivity and are developing locally. Plus, 
serving locally is much faster.

Thus, we don't need to commit these packages to the repo. The folder names 
are committed so we have an idea of what dependencies are needed. Locally 
these should be populated with the requisite files, but we use .gitignore to 
suppress committing them to the repo.
