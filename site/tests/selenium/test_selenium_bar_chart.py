"""Selenium test library for bar chart.

Author: James Hohman
Copyright James Hohman 2015
"""
from selenium.webdriver.firefox.webdriver import WebDriver  # @UnresolvedImport
from django.contrib.staticfiles.testing import (
    StaticLiveServerTestCase  # @UnresolvedImport
    )
from nose.plugins.attrib import attr  # @UnresolvedImport
from django.core.urlresolvers import reverse


@attr(selenium_tests=True)
class D3BarChartTests(StaticLiveServerTestCase):
    '''Selenium test class to test the d3 frequency (bar chart)
    functionality.'''
    @classmethod
    def setUpClass(cls):
        super(D3BarChartTests, cls).setUpClass()
        cls.selenium = WebDriver()

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(D3BarChartTests, cls).tearDownClass()

    def test_bar_y_labels_minimum(self):
        '''Test the y-axis transform offset.
        Issue #8:
        https://bitbucket.org/TechMedicNYC/textparseexample/issue/
          8/d3-frequency-chart-y-axis-offset-for-short
        '''
        # Input 1-character text strings to the parser and submit.
        self.selenium.get(self.live_server_url + reverse('textparser:index'))
        text_input = self.selenium.find_element_by_id('id_text')
        text_input.send_keys('a b c d e f g')
        # submit button.
        self.selenium.find_element_by_xpath("//button[@type='submit']").click()

        # verify page redirect to results
        self.assertEqual(self.selenium.current_url,
                         u'http://localhost:8081/textparser/results/')
        heading = self.selenium.find_element_by_class_name('cover-heading')
        self.assertEqual(heading.text, u'Results')

        # activate the frequency chart
        frequency_button = self.selenium.find_element_by_id('ctrl-d3-freq')
        frequency_button.click()

        # get the y-axis stuff
        svg = self.selenium.find_element_by_id('d3-chart-svg')
        y_axis = svg.find_element_by_css_selector('g.y.axis')
        y_trans = y_axis.get_attribute('transform')

        # We are in test, we assume eval is okay to use.
        y_trans_coords = eval(y_trans[y_trans.find('('):])
        ''''The y-axis transform in horizontal direction should be
        greater than 0 because we adjust for non-zero length word.
        Assert: y-axis offset > 0px.'''
        self.assertGreater(y_trans_coords[0], 0)
