# textParseExample
## A Python/Django/d3.js Example Web Site
## June 2015
## Copyright (C) James Hohman, 2015
----------------------------------------------

Author:

  * James Hohman
  
Requires:

  * Python 2.7.7
  * Django 1.8.2
  * Bootstrap 3.3.4
  * jQuery 1.11.3

## Description
----------------------------------------------
Simple example Python/Django site to parse text and present some pretty d3.js 
visualizations. Responsive web layout via Bootstrap, powered by Python and 
Django, JavaScript, jQuery, and d3.js.


## License
----------------------------------------------
You are free to download, modify, redistribute... basically do whatever you 
want with this code, so long as you give acknowledgement to the author and 
that this code was the original prototype.


## Quick Start
----------------------------------------------
**Without Libraries:**

If you just want to fire up the `textParseExample` site *without downloading* all the **required libraries**, then you can execute the server using the following:

1. Pull the code from the `textParseExample` repo.
2. Preferably in a `virtualenv`, install dependencies:
`pip install -r requirements.txt`
3. Start the web server:
`python manage.py runserver --settings=base_site.settings.production --insecure`

**With Libraries:**

If you pull the libraries and deploy them to the `static_externals` folder then you can just start the development server normally via:
`python manage.py runserver`


## Testing
----------------------------------------------
After installing all requirements you may execute the test suites as follows:

  * To only execute Selenium tests:
    `python manage.py test --settings=base_site.settings.selenium`
  * To only execute Django unit tests:
    `python manage.py test --settings=base_site.settings.nose`
  * To generate the coverage report (for all tests):
    (To generate coverage report for only unit tests, use the `--settings` parameter as above.)
    1. `coverage run manage.py test`
	2. `coverage report -m`
	3. `coverage html`
	4. Open `htmlcov/index.html` for coverage report.
	